<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin</title>
        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            #page-wrapper{
                margin:1px 1px 1px 1px!important; 
            }
        </style>
    </head>
    <body>
        <?php
        include("db/db.php");
        $id= $_GET["id"];
        $sql = "SELECT * FROM patient where PatientId='$id'";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        ?>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10" style="text-align:center">
                                        <h1 >Medisus Clinic <br><small>Patient Prescription</small></h1>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px">
                                    <div class="col-md-2"><b>Name Of Patient</b></div>
                                    <div class="col-md-2"><span class="text-danger"><?php echo $row["FullName"]; ?></span> </div>
                                    <div class="col-md-2"><b>Gender</b></div>
                                    <div class="col-md-2"><span class="text-danger"><?php echo $row["Gender"]; ?></span></div>
                                    <div class="col-md-2"><b>Blood</b></div>
                                    <div class="col-md-2"><span class="text-danger"><?php echo $row["Blood"]; ?></span></div>
                                </div>
                                <div class="row" style="padding-top:20px">
                                    <div class="col-md-2"><b>Height : </b><?php echo $row["height"]; ?></div>
                                    <div class="col-md-2"><b>Weight :</b><?php echo $row["weight"]; ?></div>
                                    <div class="col-md-2"><b>Blood Pressure:</b><?php echo $row["blood_pressure"]; ?></div>
                                    <div class="col-md-2"><b>Allergy:</b><?php echo $row["allergy"]; ?></div>
                                    <div class="col-md-2"><b>Sugar:</b><?php echo $row["blood_sugar"]; ?></div>
                                    <div class="col-md-2"><b>Medication:</b><?php echo $row["medication"]; ?></div>
                                </div>
                                <div class="row" style="padding-top:20px">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Prescription History
                                        </div>
                                        <?php
                                        $id= $_GET["id"];
                                        $sql = "SELECT * FROM prescription where PatientId='$id'";
                                        $result = $conn->query($sql);
                                        while($row = $result->fetch_assoc()){
                                        ?>
                                        <div class="panel-body">
                                            <p class="lead"><?php echo $row["PrescriptionTitle"]; ?></p>
                                            <p><?php echo $row["PrescriptionDescription"]; ?></p>
                                            <h5>Date:
                                            <small><?php echo $row["PrescriptionDate"]; ?></small>
                                            </h5>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>