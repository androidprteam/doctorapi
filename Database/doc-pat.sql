-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2018 at 03:53 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doc-pat`
--

-- --------------------------------------------------------

--
-- Table structure for table `appoinment`
--

CREATE TABLE `appoinment` (
  `AppoinmentId` int(10) NOT NULL,
  `DocId` int(10) NOT NULL,
  `PatientId` int(10) NOT NULL,
  `PatientName` varchar(100) NOT NULL,
  `BookingDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appoinment`
--

INSERT INTO `appoinment` (`AppoinmentId`, `DocId`, `PatientId`, `PatientName`, `BookingDate`) VALUES
(1, 0, 0, '', '0000-00-00'),
(2, 1, 1, 'sumith', '0000-00-00'),
(3, 1, 1, 'sumith', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `DocId` int(11) NOT NULL,
  `DocName` varchar(200) NOT NULL,
  `DocGen` varchar(100) NOT NULL,
  `DocAge` varchar(10) NOT NULL,
  `DocAdd` varchar(100) NOT NULL,
  `DocSpec` varchar(200) NOT NULL,
  `DocExp` varchar(50) NOT NULL,
  `DocEmail` varchar(20) NOT NULL,
  `DocUname` varchar(50) NOT NULL,
  `DocPhNo` varchar(20) NOT NULL,
  `DocStaus` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`DocId`, `DocName`, `DocGen`, `DocAge`, `DocAdd`, `DocSpec`, `DocExp`, `DocEmail`, `DocUname`, `DocPhNo`, `DocStaus`) VALUES
(1, 'jgjg', 'jgjg', 'jgjg', 'jgj', 'gjg', 'jgjg', 'jgj', '', 'gj', 1),
(2, '', '', '', '', '', '', '', '', '', 1),
(3, 'JHH', 'Male', 'JH', 'HJHJH', 'JHJ', 'HJHJ', 'HJH', '', 'JHJ', 1),
(4, 'khk', 'Male', 'kh', 'khkh', 'khk', 'hkh', 'khkh', '', 'khk', 1),
(5, 'ljj', '', '', 'ljlj', '', '', 'jljlj', 'ljlj', 'jljlj', 1),
(6, 'kh', 'Male', 'hkh', 'khkhk', 'hkh', 'khkh', 'khkh', 'kkhk', 'khkh', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lab`
--

CREATE TABLE `lab` (
  `LabId` int(10) NOT NULL,
  `LabName` varchar(100) NOT NULL,
  `LabAddress` varchar(100) NOT NULL,
  `EmailId` varchar(100) NOT NULL,
  `PhoneNo` varchar(100) NOT NULL,
  `uname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab`
--

INSERT INTO `lab` (`LabId`, `LabName`, `LabAddress`, `EmailId`, `PhoneNo`, `uname`) VALUES
(1, '', '', '', '', ''),
(2, 'kjk', 'hkhh', 'khkhk', 'hkhk', 'hkh');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `LoginId` int(11) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`LoginId`, `uname`, `password`, `role`) VALUES
(1, 'ljlj', 'ljlj', 'DOC'),
(2, 'kkhk', 'khkhk', 'DOC'),
(3, 'doc', 'kh', 'DOC');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `PatientId` int(11) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Marital` varchar(50) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `Phone` varchar(100) NOT NULL,
  `Blood` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`PatientId`, `FullName`, `Gender`, `Marital`, `Address`, `Phone`, `Blood`) VALUES
(1, '', '', '', '', '', ''),
(2, 'sumith s nair', 'male', 'married', 'vaikom', '9656761101', 'a'),
(3, 'sumith s nair', 'male', 'married', 'vaikom', '9656761101', 'a'),
(4, 'sumith s nair', 'male', 'married', 'vaikom', '9656761101', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `PrescriptionId` int(10) NOT NULL,
  `PrescriptionTitle` varchar(200) NOT NULL,
  `PrescriptionDescription` text NOT NULL,
  `PrescriptionDate` varchar(20) NOT NULL,
  `PatientId` int(10) NOT NULL,
  `DocId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`PrescriptionId`, `PrescriptionTitle`, `PrescriptionDescription`, `PrescriptionDate`, `PatientId`, `DocId`) VALUES
(0, '', '', '', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appoinment`
--
ALTER TABLE `appoinment`
  ADD PRIMARY KEY (`AppoinmentId`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`DocId`);

--
-- Indexes for table `lab`
--
ALTER TABLE `lab`
  ADD PRIMARY KEY (`LabId`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`LoginId`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`PatientId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appoinment`
--
ALTER TABLE `appoinment`
  MODIFY `AppoinmentId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `DocId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lab`
--
ALTER TABLE `lab`
  MODIFY `LabId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `LoginId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `PatientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
