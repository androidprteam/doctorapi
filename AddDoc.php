<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin</title>
        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <!-- /.navbar-header -->
                
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                       <?php include("AdminMenu.php") ?>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Doctor</h1>
                        </div>
                        <div class="col-lg-12">
                            <form role="form" action="docAction.php" method="POST">
                                <div class="form-group"  >
                                    <label>Doc Name</label>
                                    <input class="form-control" type="text" name="txtName">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select class="form-control" name="cmdGender"> 
                                        <option value="Male">Male</option>
                                        <option value="FeMale">FeMale</option>
                                    </select>
                                   
                                </div>
                                 <div class="form-group">
                                    <label>Doc Age</label>
                                    <input class="form-control" type="text" name="txtAge">
                                </div>

                                <div class="form-group">
                                    <label>Doc Address</label>
                                    <input class="form-control" type="text" name="txtAddress">
                                </div>

                                 <div class="form-group">
                                    <label>Doc Specialization</label>
                                    <input class="form-control" type="text" name="txtSpec">
                                </div>

                                 <div class="form-group">
                                    <label>Doc Experience</label>
                                    <input class="form-control" type="text" name="txtExp">
                                </div>
                                <div class="form-group">
                                    <label>Doc Email Id</label>
                                    <input class="form-control" type="text" name="txtEmail">
                                </div>
                                <div class="form-group">
                                    <label>Doc Phone No</label>
                                    <input class="form-control" type="text" name="txtPhno">
                                </div>
                                <div class="form-group">
                                    <label>UserName</label>
                                    <input class="form-control" type="text" name="txtUsername">
                                </div>
                                 <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="text" name="txtPwd">
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="btnSub" value="Save" class="btn btn-default">
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="vendor/metisMenu/metisMenu.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>
    </body>
</html>