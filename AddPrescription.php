<?php 
session_start(); 
include("db/db.php");
$docName = $_SESSION["uname"];
$sql = "SELECT * FROM doctor where DocUname='$docName'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$doc_id=$row["DocId"];
$patient_id=$_GET["id"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Doctor Home</title>
        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <!-- /.navbar-header -->
                
                <?php include("DocMenu.php") ?>
                
            </nav>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Prescription</h1>
                           	<div class="col-lg-12">
                                    <form role="form" action="PrescriptionAction.php" method="post">
                                        <div class="form-group">
                                            <label>Prescription Title</label>
                                            <input class="form-control" name="txtTitle">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="3" name="txtDescription"></textarea>
                                            <input type="hidden" name="hdPatientId" value="<?php echo $patient_id; ?> ">
                                            <input type="hidden" name="hdDocId" value="<?php echo $doc_id; ?> ">
                                        </div>
                                       
                                        <button type="submit" class="btn btn-default">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                         </div>
                         <div class="row">

                            <div class="col-lg-12">
                            	<h1 class="page-header">View Prescription History</h1>
                            	<table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Date </th>
                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                   
                                    $sql = "SELECT * FROM prescription where PatientId='$patient_id'";
                                    $result = $conn->query($sql);
                                    while($row = $result->fetch_assoc()) {
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $row["PrescriptionTitle"]; ?></td>
                                        <td><?php echo $row["PrescriptionDescription"]; ?></td>
                                        <td><?php echo $row["PrescriptionDate"]; ?></td>
                                        
                                        
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="vendor/metisMenu/metisMenu.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>
    </body>
</html>